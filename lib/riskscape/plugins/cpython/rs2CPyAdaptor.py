#!/usr/bin/env python3
#
# Main CPython entry-point, called directly from RiskScape.
#
# This code serves as a long-running IPC 'bridge' between RiskScape (Java) and
# the user's CPython function. It takes serialized arguments from a RiskScape
# function call, deserializes the data into Python types, and then passes the
# arguments to the user's Cpython function.
#
# This script gets passed the CPython function's details as text args when it
# first gets spawned by RiskScape, e.g.
#    rs2CPyAdaptor my_function.py Text Floating
#
import os
import sys
import logging
import traceback
from inspect import signature, Parameter
import serializer
from serializer import Boolean, RiskscapePyException, Text


def check_callable(x, name, expected_args):
    if not callable(x):
        raise RiskscapePyException("'" + str(name) +
            "' is not a callable function")
    # sanity-check the number of args is sensible
    params = signature(x).parameters
    positional_args = 0
    total_args = 0
    for p in params.values():
        # *args case: user is advanced enough we can just leave them alone
        if p.kind == Parameter.VAR_POSITIONAL:
            return
        if p.kind == Parameter.POSITIONAL_OR_KEYWORD:
            total_args += 1
        if p.default == Parameter.empty:
            positional_args += 1

    # give the user a nudge if their python args don't line up with the INI file
    if expected_args < positional_args:
        raise RiskscapePyException("Your python function takes {0} positional "
            "arguments, but your INI file specifies {1} 'argument-types' for "
            "this function. Please ensure the argument-types in the INI file "
            "match your Python code.".format(positional_args, expected_args))
    if total_args < expected_args:
        raise RiskscapePyException("Your python function takes {0} positional "
            "or keyword arguments, but your INI file specifies {1} "
            "'argument-types' for this function. Please ensure the "
            "argument-types in the INI file match your "
            "Python code.".format(total_args, expected_args))


# comment out this next line for debugging to file
#logging.basicConfig(filename='debug.txt', level=logging.DEBUG)
logger = logging.getLogger('RiskScape')

# first arg is the filepath containing the user's python function
filepath = sys.argv[1]
# second arg is the function's return-type
return_type = sys.argv[2]
# last comes the types that the function takes as args
arg_types = sys.argv[3:]

# dynamically import the user function
# First, we need to add the dir that the file is in to our path
module_dir = os.path.abspath(os.path.dirname(filepath))
# we prepend so we try to load the user's file first. This prioritizes loading
# their filename over existing python modules. Otherwise, calling the file
# 'test.py' causes problems because there's already a built-in 'test' python
# module
sys.path.insert(0, module_dir)

# then import the python file itself
filename_no_ext = os.path.splitext(os.path.basename(filepath))[0]
try:
    function = __import__(filename_no_ext)

    # sanity-check there's a callable function in this file we can use
    if hasattr(function, 'FUNCTION'):
        check_callable(function.FUNCTION, 'FUNCTION', len(arg_types))
        user_function = function.FUNCTION
    elif hasattr(function, 'function'):
        check_callable(function.function, 'function', len(arg_types))
        user_function = function.function
    else:
        raise RiskscapePyException("No 'def function' present in " +
            filepath + ". Please name the method you want to execute "
            "'function'.")

    # RiskScape will start serializing the args for each function call directly
    # to our stdin
    input = sys.stdin.buffer

    # We serialize the function call return-value/result to our stdout
    with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as output:

        # we don't want user print() statements messing with our serialization, so set stdout to be stderr, too.
        # this way the user gets to see *something* (and they're not likely to care about the distinction when
        # running their scripts with RiskScape
        sys.stdout = sys.stderr

        try:
            import fcntl
            # get full buffer to throw an exception instead of blocking on full stderr - this is better than a
            # deadlock.  Note that we won't be able to (easily) give the user an error message, as stderr is
            # full and won't be able to accept a message.  We could swap sys.stderr for a StringIO object and
            # manage the buffer ourselves, but if we take care on the Java side to read the bytes from stderr
            # as they become available, then this becomes very unlikely to happen
            errfd = sys.stderr.fileno()
            curr_flags = fcntl.fcntl(errfd, fcntl.F_GETFL)
            fcntl.fcntl(errfd, fcntl.F_SETFL, curr_flags | os.O_NONBLOCK)
        except:
            pass # ah well, we tried

        # tell riskscape we've come up OK before we start looping
        Boolean.toBytes(output, True, None)
        output.flush()

        while True:
            # read the input args that the main Java process passed to us via stdin
            args = []
            for type_name in arg_types:
                data = serializer.read(type_name, input)
                logger.debug("Received {0} arg: {1}".format(type_name, data))
                args.append(data)


            result = None
            try:
                result = user_function(*args)
            except Exception as e:
                logger.debug("Failed to call user function : {0}".format(e))

                # tell java to expect an error message
                Boolean.toBytes(output, False, None)
                output.flush()

                # Format a stack trace, omitting the current line of execution (which is
                # this wrapper script, which the user won't know about)
                tb_string = '\n'.join(traceback.format_tb(e.__traceback__.tb_next))

                # Now format the exception part - nb exception comes from python internals
                exc_string = traceback.format_exc(limit=0)

                # send 'em to the waiting arms of the JVM
                Text.toBytes(output, exc_string + '\n' + tb_string, None)
                output.flush()

                # wait for the next method call, maybe it'll work this time?
                continue

            logger.debug("Returning result: {0}".format(result))

            # Write the return value back to the RiskScape Java process,
            # NB we write true for success so that java will expect to deserialize the serialized result
            # and not an exception
            Boolean.toBytes(output, True, None)
            output.flush()
            serializer.write(return_type, output, result)

            # flush streams between invocations - we want java to get it all
            output.flush()
            sys.stderr.flush()

except RiskscapePyException as rse:
    # we hit an error in the serialization code. The traceback is probably
    # completely meaningless to the user, so just display the error message
    print('Error: %s' % rse, file=sys.stderr)
    exit(1)
except Exception as e:
    # we hit an error outside of the user's code that wasn't a serialization failure.
    # This is very likely to be an internal error that only a dev will be able to solve.
    # They will probably see the last thing that was printed to stderr. They can then
    # put that in a bug report - result!
    traceback.print_tb(e.__traceback__.tb_next, file=sys.stderr)
    # Now print out the exception without any stack trace.
    # Note Python works the exception here from the current 'except' case.
    # We could use other traceback APIs here, but they've changed a bit
    # between 3.5 and 3.10, so might cause compatability problems
    traceback.print_exc(limit=0, file=sys.stderr)
    exit(1)
