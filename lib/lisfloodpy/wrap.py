#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as op
import sys
import subprocess as sp

import numpy as np
import xarray as xr

from .io import LisfloodIO
from .plots import plot_dem_preprocessed, plot_dem_raw


class LisfloodCase(object):
    'LISFLOOD-FP case input definition'

    def __init__(self):
        '''
        This class stores LISFLOOD-FP case input information
        '''

        # case ID
        self.id = None

        # constant inflow
        self.qfix_activate  = False
        self.qfix_xcoord    = None   # x coordinates (int list / 1D np array)
        self.qfix_ycoord    = None   # y coordinates (int list / 1D np array)
        self.qfix_value     = None   # inflow value (float)

        # time varying inflow
        self.qvar_activate  = False
        self.qvar_profile   = None   # inflow profiles (int list / 1D np array)
        self.qvar_xcoord    = None   # x coordinates (int list / 1D np array)
        self.qvar_ycoord    = None   # y coordinates (int list / 1D np array)
        self.qvar_freq      = None   # inflow time frequency (HOURS, ...)
        self.qvar_value     = None   # inflow values (xarray.DataArray. coords: profile, time)
                                     # inflow units: m3/s*m (UTM coordinates)

        # constant water level
        self.hfix_activate  = False
        self.hfix_value     = None   # water level value (float)
        self.hfix_xcoord    = None   # x coordinates (int list / 1D np array)
        self.hfix_ycoord    = None   # y coordinates (int list / 1D np array)

        # time varying water level
        self.hvar_activate  = False
        self.hvar_profile   = None   # level profiles (int list / 1D np array)
        self.hvar_xcoord    = None   # x coordinates (int list / 1D np array)
        self.hvar_ycoord    = None   # y coordinates (int list / 1D np array)
        self.qvar_freq      = None   # level time frequency (HOURS, ...)
        self.hvar_value     = None   # level values (xarray.DataArray. coords: profile, time)
                                     # level units: m 

        # rain
        self.rain_activate  = False
        self.rain_freq      = None   # rain time frequency (HOURS, ...)
        self.rain_value     = None   # rain values (xarray.DataArray. coords: time)
                                     # rain units: mm/h 
        
        # level
        self.level_freq = None
        self.level_value = None
         

class LisfloodProject(object):
    '''
    LISFLOOD-FP flood model project parameters.
    '''

    def __init__(self, p_proj, n_proj):
        '''
        This class stores LISFLOOD-FP project information
        '''

        self.p_main = op.join(p_proj, n_proj)    # project path
        self.name = n_proj                       # project name

        # sub folders
        self.p_cases = op.join(self.p_main)
        self.dem_file = None
        self.bci_file = None
        self.cartesian = None
        
        # .DEM file
        self.ncols = None
        self.nrows = None
        self.xllcorner = None
        self.yllcorner = None
        self.cellsize = None
        self.nondata = None
        
        # .par file parameters
        self.PARfile = 'params.par' # parameters file
        self.DEMfile = 'DEM.asc'    # digital elevation model filename
        self.resroot = 'out'        # root for naming the output files
        self.dirroot = 'outputs'    # name for results files folder
        self.saveint = None         # interval for saved files (in seconds)
        self.massint = None         # interval for .mass file (in seconds)
        self.sim_time = None        # simulation length (in seconds)
        self.initial_tstep = None   # model time step (in seconds)
        self.fpfric = None          # Manning's value (floodplain) if spatially uniform
        self.manningfile = None     # filename for grid Manning values

        # .bci file parameters
        self.bcifile = 'boundary.bci'  # boundary condition filename (.bci)

        # .bdy file parameters
        self.bdyfile = 'boundary.bdy'  # varying boundary conditions filename (.bdy)

        # .rain file parameteres
        self.rainfile = 'rainfall.rain'  # rainfall conditions filename (.rain)

        # .start file
        self.start = False           # activation of .start
        self.startfile = 'st.start'  # start conditions filename (.start)

        self.solver = None          # model solver

        # water parameters
        self.routing = False        # RECOMMENDED FOR RAINFALL WHEN STEEP SLOPES
        self.depththresh = None     # depth at which a cell is considered wet (m)
        self.routesfthresh = None   # Water surface slope above which routing occurs
        self.routingspeed = None
        self.infiltration = None    # (m/s) uniform

        # optional additions
        self.theta = None           # reset numerical diffusion for 'acceleration'
        self.latlong = None         # only for Subgrid and 2D

        # additional output settings
        self.overpass = None        # time for flood image available (s) ??????
        self.depthoff = False       # suppress depth files (*.wd) 
        self.elevoff = False        # suppress water surface files (*.elev)
        self.mint_hk = None         # allows calculation of maxH... at the mass interval instead of every tstep


class LisfloodWrap(object):
    'LISFLOOD-FP flood model wrap for multi-case handling'

    def __init__(self, lisflood_proj):
        '''
        lisflood_proj - LisfloodProject() instance, contains project parameters
        '''

        self.proj = lisflood_proj           # lisflood project parameters
        self.io = LisfloodIO(self.proj)     # lisflood input/output 
        self.path_case = None
        
        # lisflood executable
        self.p_bin = op.join(op.dirname(op.realpath(__file__)), 'resources', 'bin')

    def build_cases(self, list_cases):
        '''
        Generates all files needed for a list of Lisflood cases

        list_cases     - list of LisfloodCase instances.
        '''

        # make main project directory
        self.io.make_project()

        # build each case
        for lc in list_cases:
            self.io.build_case(lc)

    def get_run_folders(self):
        'Return sorted list of project cases folders'

        ldir = sorted(os.listdir(self.proj.p_cases))
        fp_ldir = [op.join(self.proj.p_cases, c) for c in ldir]

        return [p for p in fp_ldir if op.isdir(p)]

    def run_cases(self):
        'run all cases inside project "cases" folder'
        
        # get sorted execution folders
        run_dirs = self.get_run_folders()
        
        # run case
        for run_dir in run_dirs:
            self.run(run_dir)

    def run(self, p_run):
        'Execution commands for launching LISFLOOD-FP'

        # TODO: linux computers can have a problem finding libiomp4.so library 
        # use next lines to fix it
        #os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
        #os.environ['LD_LIBRARY_PATH'] = r'/home/anaconda3/lib/' <-- library path

        # aux. func. for launching bash command
        def bash_cmd(str_cmd, out_file=None, err_file=None):
            'Launch bash command using subprocess library'

            _stdout = None
            _stderr = None

            if out_file:
                _stdout = open(out_file, 'w')
            if err_file:
                _stderr = open(err_file, 'w')

            s = sp.Popen(str_cmd, shell=True, stdout=_stdout, stderr=_stderr)
            s.wait()

            if out_file:
                _stdout.flush()
                _stdout.close()
            if err_file:
                _stderr.flush()
                _stderr.close()

        # parameters file
        par_file = op.join(p_run, self.proj.PARfile) 

        # lisflood args
        args = '-v -log'

        # get OS lisflood binary
        if sys.platform.startswith('win'):
            # Windows
            # TODO obtain  windows binary
            fbin = op.abspath(op.join(self.p_bin, 'lisflood.exe'))

        elif sys.platform.startswith('linux'):
            fbin = op.abspath(op.join(self.p_bin, 'lisflood_euflood_lnx'))

        elif sys.platform == 'darwin':
            fbin = op.abspath(op.join(self.p_bin, 'lisflood_euflood_osx'))

        else:
            print('LISFLOOD-FP binary not available for current OS')
            return

        print('\n Running LISFLOOD-FP case...\n')
        cmd = 'cd {0} && {1} {2} {3}'.format(p_run, fbin, args, par_file)
        bash_cmd(cmd)

    def extract_output(self, output_run='2d'):
        '''
        extract output from all cases generated by "build_cases"

        - output_run = '.mass' / '2d'

        return xarray.Dataset (uses new dim "case" to join output)
        '''

        # choose output extraction function
        if output_run == '.mass':
            fo = self.io.output_case_mass
        elif output_run == '2d':
            fo = self.io.output_case_2D
        else:
            print('not output extraction for {0} file'.format(output_run))
            return None

        # get sorted execution folders
        run_dirs = self.get_run_folders()

        # exctract output case by case and concat in list
        l_out = []
        for p_run in run_dirs:

            # read output file
            xds_case_out = fo(p_run)
            l_out.append(xds_case_out)

        # concatenate xarray datasets (new dim: case)
        xds_out = xr.concat(l_out, dim='case')

        return(xds_out)

