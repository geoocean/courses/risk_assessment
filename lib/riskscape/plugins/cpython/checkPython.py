#
# checkPython.py
#
# Script run by riskscape when the cpython plugin starts to check whether the
# interpreter is 'suitable' for use with the cpython plugin
#

import sys

if sys.version_info.major != 3:
    exit("Bad major version of python - %s" % sys.version_info)

import platform

if str(platform.python_implementation()) != 'CPython':
    exit("Unsupported python implementation - %s" % platform.python_implementation)

# all clear
exit(0)
